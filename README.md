# Bitbucket Pipelines Pipe: SonarQube Quality Gate check

Check the Quality Gate of your code with [SonarQube](https://sonarqube.org) to ensure your code meets your own quality standards before you release or deploy new features. 

[SonarQube](https://sonarqube.org) is the leading product for Continuous Code Quality & Code Security. It supports most popular programming languages, including Java, JavaScript, TypeScript, C#, Python, C, C++, and many more.

This pipe blocks the execution of your pipeline until it receives the SonarQube Quality Gate status for your commit:

- if the Quality Gate status is red, this pipe will stop the execution of the pipeline with a failure. 
- if the Quality Gate status is green, this pipe will proceed with the execution of the rest of the pipeline.

NOTE: This Bitbucket pipe is designed to work in conjunction with `sonarsource/sonarqube-scan` Bitbucket pipe, but it also works with other SonarQube scanners:

- For Maven see [Scanner for Maven](https://redirect.sonarsource.com/doc/install-configure-scanner-maven.html)
- For Gradle see [Scanner for Gradle](https://redirect.sonarsource.com/doc/gradle.html)
- For a .NET solution see [Scanner for .NET](https://redirect.sonarsource.com/doc/install-configure-scanner-msbuild.html)
- For C/C++ code see [analyzing C/C++ code](https://docs.sonarqube.org/latest/analysis/languages/cfamily/)

See [SonarQube docs](https://docs.sonarqube.org/latest/analysis/bitbucket-cloud-integration/) for more details on how to configure integration with Bitbucket Cloud.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
# It's recommended to specify the 'max-time' for the step to not wait for the default Bitbucket Pipelines timeout
# max-time: 5
- pipe: sonarsource/sonarqube-quality-gate:1.0.0
  variables:
    SONAR_TOKEN: ${SONAR_TOKEN} # Get the value from the repository/workspace variable. You shouldn't set secret in clear text in here.
    # REPORT_FILE: '<string>'  # Optional if this pipe is used after `sonarsource/sonarqube-scan` pipe. Otherwise you have to point to the analysis report file created by SonarQube scanner in a previous pipeline step.
```

## Variables

| Variable         | Usage                                                                                                                                                                                                |
| ---------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| SONAR_TOKEN (\*) | Your SonarQube access token. You should use a **secured** repository/workspace variable to store this value.                                                                                         |
| REPORT_FILE      | Path to SonarQube analysis report file (`report-task.txt`). The value you should provide depends on the scanner you used to trigger the analysis:<br/>- if you use `sonarsource/sonarqube-scan` Bitbucket pipe, there is no need to use this variable.<br/>- if you use Scanner for Maven, you should set this to `target/sonar/report-task.txt`. <br/>- if you use Scanner for Gradle, you should set this to `build/sonar/report-task.txt`.<br/>- if you use Scanner for .NET, you should set it to `.sonarqube/out/.sonar/report-task.txt`<br/>- if you are analyzing C/C++ code, you should set it to `.scannerwork/report-task.txt` |

_(\*) = required variable._

## Examples
Here is an example of using both `sonarsource/sonarqube-scan` and `sonarsource/sonarqube-quality-gate` pipes in a pipeline that builds, tests and
checks the SonarQube Quality Gate before deploying a NodeJS application to production:

```yaml
image: node:10.15.3

clone:
  depth: full # SonarQube scanner needs the full history to assign issues properly

pipelines:
  default:
    - step:
        name: Build, run tests, analyze on SonarQube
        caches:
          - node
        script:
          - npm install
          - npm test
          - pipe: sonarsource/sonarqube-scan:1.0.0
            variables:
              SONAR_HOST_URL: ${SONAR_HOST_URL}
              SONAR_TOKEN: ${SONAR_TOKEN}
    - step:
        name: Check Quality Gate on SonarQube
        max-time: 5 # value you should use depends on the analysis time for your project
        script:
          - pipe: sonarsource/sonarqube-quality-gate:1.0.0
            variables:
              SONAR_TOKEN: ${SONAR_TOKEN}
    - step:
        name: Deploy to Production
        deployment: "Production"
        script:
          - echo "Good to deploy!"
```

## Support

To get help with this pipe, or to report issues or feature requests, please get in touch on [our community forum](https://community.sonarsource.com/tags/c/help/sq/bitbucketcloud).

If you are reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
