#!/bin/bash

set -e

source "$(dirname "$0")/common.sh"

parse_environment_variables() {
  SONAR_TOKEN=${SONAR_TOKEN:?'SONAR_TOKEN variable is missing.'}
  REPORT_FILE="report-task.txt"
}

parse_environment_variables

metadataFile="${REPORT_FILE}"

if [[ ! -f "${metadataFile}" ]]; then
   echo "${metadataFile} does not exist."
   exit 1
fi

serverUrl="$(sed -n 's/serverUrl=\(.*\)/\1/p' "${metadataFile}")"
ceTaskUrl="$(sed -n 's/ceTaskUrl=\(.*\)/\1/p' "${metadataFile}")"
dashboardUrl="$(sed -n 's/dashboardUrl=\(.*\)/\1/p' "${metadataFile}")"

if [ -z "${serverUrl}" ] || [ -z "${ceTaskUrl}" ] || [ -z "${dashboardUrl}" ]; then
  echo "Invalid report metadata file."
  exit 1
fi

task="$(curl --silent --fail --show-error --user "${SONAR_TOKEN}": "${ceTaskUrl}")"
status="$(jq -r '.task.status' <<< "${task}")"

until [[ ${status} != "PENDING" && ${status} != "IN_PROGRESS" ]]; do
    echo "Waiting for Quality Gate check..."
    sleep 5
    task="$(curl --silent --fail --show-error --user "${SONAR_TOKEN}": "${ceTaskUrl}")"
    status="$(jq -r '.task.status' <<< "${task}")"
done

pullRequest="$(jq -r '.task.pullRequest' <<< "${task}")"
componentKey="$(jq -r '.task.componentKey' <<< "${task}")"
qualityGateUrlTemp="${serverUrl}/api/qualitygates/project_status?projectKey=${componentKey}&pullRequest=${pullRequest}"
qualityGateUrl="$(echo $qualityGateUrlTemp | sed  's/ //g')"
qualityGateStatus="$(curl --silent --fail --show-error --user "${SONAR_TOKEN}": "${qualityGateUrl}" | jq -r '.projectStatus.status')"

if [[ ${qualityGateStatus} == "OK" ]];then
   success "Quality Gate has PASSED, you can check more details on: ${dashboardUrl}"
elif [[ ${qualityGateStatus} == "WARN" ]];then
   warn "Warnings on Quality Gate, you can check more details on: ${dashboardUrl}"
elif [[ ${qualityGateStatus} == "ERROR" ]];then
   fail "Quality Gate has FAILED, you can check more details on: ${dashboardUrl}"
else
   fail "Quality Gate not set for the project. Please configure the Quality Gate in SonarQube or remove sonarqube-quality-gate pipe from the pipeline."
fi
